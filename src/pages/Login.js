import {Fragment, useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Login(){

	const [password, setPassword] = useState('')
	const [email, setEmail] = useState('')
	const [isActive, setIsActive] = useState('')

	function loginUser(e){

		e.preventDefault();

		// set the email of the user in the local storage
		// Syntax:
			// localStorage.setItem('propertyItem', value)
		localStorage.setItem('email', email)

		setEmail('')
		setPassword('')

		alert('You are now logged in.')
	}

	useEffect(() => {

		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password])

	return(
		<Fragment>
		<h2>Login</h2>
		<Form onSubmit = {(e) => loginUser(e)}>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type= 'email'
					placeholder= 'Enter email address'
					value= {email}
					onChange= {e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type= 'password'
					placeholder= 'Enter password'
					value= {password}
					onChange= {e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ? 
				<Button variant = 'success' type = 'submit' id = 'submitBtn'>
					Submit
				</Button>

				:
				<Button variant = 'danger' type = 'submit' id = 'submitBtn' disabled>
					Submit
				</Button>
			}
		</Form>
		</Fragment>
	)
}