import { Redirect } from 'react-router-dom'

export default function Logout(){

	localStorage.clear()

	// Redirect allows us to navigate to a new location
	return (

		<Redirect to='/login' />

		)

}