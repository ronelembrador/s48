import {useState} from 'react'
import {Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}) {

console.log(courseProp)

// Object Destructuring
const {name, description, price} = courseProp
// Syntax: const {properties} = propname

// Array Destructuring
const [count, setCount] = useState(0)
// Syntax: const [getter, setter] = useState(initialValue)

// Hook used is useState- to store the state

const [seats, setSeats] = useState(30)

function enroll(){
	if(seats > 0) {
		setSeats(seats - 1)
		setCount(count + 1);
		console.log('Enrollees' + count)
	} else {
		alert("no more seats")
	}
}



	return(
		<Card>
			<Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Button variant="primary" onClick = {enroll}>Enroll</Button>
            </Card.Body>
		</Card>

	)
}