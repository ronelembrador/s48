//import {Fragment} from 'react';
import {Container} from 'react-bootstrap';
import AppNavBar from './components/AppNavBar'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses';
import NotFound from './pages/NotFound';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';

function App() {
  return (
      <Router>
        <AppNavBar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/logout" component={Logout} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Container>
      </Router>
  );
}

export default App;


/*
  
  ReactJS is a single page application(SPA). However, we can simulate the changing of pages. We don't actually create new pages, what we just do is switch pages according to their assigned routes. ReactJS and react-router-dom package just mimics or mirrors how HTML access its URL.


  react-router-dom 3 main components to simulate the changing of page

  1. Router - wrapping the router component around other components will allow us to use routing within our page.
  2. Switch - Allow us to switch/ change our page components
  3. Route - assigns a path which will trigger the change/switch of components render.

*/
